locals {
  scaling_resource_name_prefix  = "${var.app_name}-${var.environment}"
}

#--------------------------------------------------------------
# Launch Configuration
#--------------------------------------------------------------
resource "aws_launch_configuration" "app" {
  name_prefix          = "${local.scaling_resource_name_prefix}-"
  image_id             = var.ami
  key_name             = var.keypair
  instance_type        = var.instance_type
  ebs_optimized        = var.ebs_optimized
  associate_public_ip_address = false
  security_groups = [join(",",aws_security_group.app[*].id)]
  user_data = var.user_data_template

  root_block_device {
    volume_type           = var.rootvol_type
    volume_size           = var.rootvol_size
    delete_on_termination = var.rootvol_delete
  }

  ebs_block_device {
    device_name = "/dev/sdh"
    volume_type = "gp2"
    volume_size = 20
  }

  lifecycle {
    create_before_destroy = true
  }
}