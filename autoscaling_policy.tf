locals {
  enable_autoscaling_scale_up_policy                                     = min(var.enable_autoscaling ? 1 : 0,var.enable_autoscaling_scale_up_policy ? 1 : 0)
  create_autoscaling_simple_scale_up_policy                              = var.autoscaling_scale_up_autoscaling_policy_type == "SimpleScaling" && var.autoscaling_scale_up_autoscaling_policy_adjustment_type != "PercentChangeInCapacity" ? local.enable_autoscaling_scale_up_policy : 0
  create_autoscaling_simple_scale_up_percent_change_in_capacity_policy   = var.autoscaling_scale_up_autoscaling_policy_type == "SimpleScaling" && var.autoscaling_scale_up_autoscaling_policy_adjustment_type == "PercentChangeInCapacity" ? local.enable_autoscaling_scale_up_policy : 0

  enable_autoscaling_scale_down_policy                                   = min(var.enable_autoscaling ? 1 : 0,var.enable_autoscaling_scale_down_policy ? 1 : 0)
  create_autoscaling_simple_scale_down_policy                            = var.autoscaling_scale_down_autoscaling_policy_type == "SimpleScaling" && var.autoscaling_scale_down_autoscaling_policy_adjustment_type != "PercentChangeInCapacity" ? local.enable_autoscaling_scale_down_policy : 0
  create_autoscaling_simple_scale_down_percent_change_in_capacity_policy = var.autoscaling_scale_down_autoscaling_policy_type == "SimpleScaling" && var.autoscaling_scale_down_autoscaling_policy_adjustment_type == "PercentChangeInCapacity" ? local.enable_autoscaling_scale_down_policy : 0

}

#--------------------------------------------------------------
# Simple Scale Up
#--------------------------------------------------------------
resource "aws_autoscaling_policy" "simple_scale_up" {
  count                     = local.create_autoscaling_simple_scale_up_policy
  autoscaling_group_name    = local.asg_name
  name                      = "${local.scaling_resource_name_prefix}-${var.autoscaling_scale_up_autoscaling_policy_name_suffix}"
  adjustment_type           = var.autoscaling_scale_up_autoscaling_policy_adjustment_type
  policy_type               = var.autoscaling_scale_up_autoscaling_policy_type
  cooldown                  = var.autoscaling_scale_up_autoscaling_policy_cooldown
  scaling_adjustment        = var.autoscaling_scale_up_autoscaling_policy_scaling_adjustment
}

resource "aws_autoscaling_policy" "simple_scale_up_percent_change_in_capacity" {
  count                     = local.create_autoscaling_simple_scale_up_percent_change_in_capacity_policy
  autoscaling_group_name    = local.asg_name
  name                      = "${local.scaling_resource_name_prefix}-${var.autoscaling_scale_up_autoscaling_policy_name_suffix}"
  adjustment_type           = var.autoscaling_scale_up_autoscaling_policy_adjustment_type
  policy_type               = var.autoscaling_scale_up_autoscaling_policy_type
  cooldown                  = var.autoscaling_scale_up_autoscaling_policy_cooldown
  scaling_adjustment        = var.autoscaling_scale_up_autoscaling_policy_scaling_adjustment
  #min_adjustment_magnitude is only applicable for PercentChangeInCapacity and cannot be set to empty string - "".
  min_adjustment_magnitude  = var.autoscaling_scale_up_autoscaling_policy_min_adjustment_magnitude
}

#--------------------------------------------------------------
# Simple Scale Down
#--------------------------------------------------------------
resource "aws_autoscaling_policy" "simple_scale_down" {
  count                     = local.create_autoscaling_simple_scale_down_policy
  autoscaling_group_name    = local.asg_name
  name                      = "${local.scaling_resource_name_prefix}-${var.autoscaling_scale_down_autoscaling_policy_name_suffix}"
  adjustment_type           = var.autoscaling_scale_down_autoscaling_policy_adjustment_type
  policy_type               = var.autoscaling_scale_down_autoscaling_policy_type
  cooldown                  = var.autoscaling_scale_down_autoscaling_policy_cooldown
  scaling_adjustment        = var.autoscaling_scale_down_autoscaling_policy_scaling_adjustment
}

resource "aws_autoscaling_policy" "simple_scale_down_percent_change_in_capacity" {
  count                     = local.create_autoscaling_simple_scale_down_percent_change_in_capacity_policy
  autoscaling_group_name    = local.asg_name
  name                      = "${local.scaling_resource_name_prefix}-${var.autoscaling_scale_down_autoscaling_policy_name_suffix}"
  adjustment_type           = var.autoscaling_scale_down_autoscaling_policy_adjustment_type
  policy_type               = var.autoscaling_scale_down_autoscaling_policy_type
  cooldown                  = var.autoscaling_scale_down_autoscaling_policy_cooldown
  scaling_adjustment        = var.autoscaling_scale_down_autoscaling_policy_scaling_adjustment
  #min_adjustment_magnitude is only applicable for PercentChangeInCapacity and cannot be set to empty string - "".
  min_adjustment_magnitude  = var.autoscaling_scale_down_autoscaling_policy_min_adjustment_magnitude
}