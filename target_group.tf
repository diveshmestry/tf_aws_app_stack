#--------------------------------------------------------------
# Target Group
#--------------------------------------------------------------
resource "aws_alb_target_group" "app" {
  count    = var.create_lb_target_group || var.create_lb ? 1 : 0
  port     = var.target_group_port
  protocol = var.target_group_protocol
  vpc_id   = var.vpc
}
