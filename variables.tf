variable "actions_enabled_true" {
  default = "true"
}



variable "all_cidr_block" {
  default = ["0.0.0.0/0"]
}

variable "ami" {
  description = "The AMI ID the ASG should use when launching instances."
}

variable "app_ebs_mount" {
  default = "/application"
}

variable "app_ebs_size" {
  default = 100
}

variable "app_ebs_type" {
  default = "gp2"
}

variable "app_egress_cidr_blocks" {
  description = "The list of application / EC2 egress cidr blocks."
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "app_egress_from_port" {
  description = "The inclusive application / EC2 egress starting port."
  default     = 0
}

variable "app_egress_protocol" {
  description = "The application / EC2 egress protocol."
  default     = -1
}

variable "app_egress_to_port" {
  description = "The application / EC2 egress inclusive ending port."
  default     = 65535
}

variable "app_name" {
  description = "The name of the application to be deployed."
}

variable "app_repo" {
  description = "The application repository to apply to the launch configuration user data."
}

variable "app_shared_security_group_ids" {
  description =  "A list of security group id to attach to the EC2.  This should be shared / commmon across similar applications."
  type        = string
  default     = ""
}

variable "app_version" {
  description = "The application version to apply to the launch configuration user data."
}

variable "asg_autospot_enabled" {
  description = "A tag to enable or disable autospot eligibility for this Auto Scaling Group."
  default     = "false"
}

variable "asg_capacity_timeout" {
  default     = "20m"
  description = "The key pair name the ASG should assign when launching instances."
}

variable "asg_comparison_operator" {
  default = "LessThanThreshold"
}

variable "asg_desired_capacity" {
  default = null
}

variable "asg_enabled_metrics" {
  type        = list(string)
  default     = ["GroupInServiceInstances"]
  description = "A list of enabled metrics for the auto scaling group."
}

variable "asg_evaluation_periods" {
  default = 10
}

variable "asg_grace_period" {
  default     = 600
  description = "The amount of time in seconds after an instance comes into service before checking health."
}

variable "asg_health_check_type" {
  default     = "EC2"
  description = "The autoscaling group health check type."
}

variable "asg_max" {
  description = "The maximum number of instances the ASG should launch."
}

variable "asg_min" {
  description = "The minimum number of instances the ASG should launch."
}


variable "asg_period" {
  default = 60
}

variable "asg_rolling_deploy" {
  default = false
  description = "If true ASG will preform a rolling deploy."
}

variable "asg_runbook_code" {
  default = "301"
}

variable "asg_statistic" {
  default = "Average"
}

variable "asg_subnets" {
  description = "Comma separated list of subnet IDs in which the ASG should launch resources.  If application uses a persistent EBS volume then only 1 AZ should be used.  This paramater should be synchronized with availability_zones."
}

variable "asg_suspended_processes" {
  type        = list(string)
  default     = []
  description = "A list of suspended processes for the auto scaling group."
}

variable "asg_threshold" {
  default = 1
}

variable "asg_treat_missing_data" {
  default = "missing"
}

variable "autoscaling_metric_name" {
  default = "GroupInServiceInstances"
}

variable "autoscaling_namespace" {
  default = "AWS/AutoScaling"
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_actions_enabled" {
  description = "Actions enabled of the scale down cloudwatch metric alarm."
  default     = true
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_comparison_operator" {
  description = "Comparison Operator of the scale down cloudwatch metric alarm.  Valid values: GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold."
  default     = "LessThanThreshold"
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_datapoints_to_alarm" {
  description = "Datapoints to alarm of the scale down cloudwatch metric alarm."
  default     = 2
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_description" {
  description = "Description of the scale down cloudwatch metric alarm."
  default     = "When breached (in alarm state) triggers the auto scaling group remove ec2 instances."
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_dimensions" {
  description = "Dimensions of the scale down cloudwatch metric alarm.  This is a key value map.  Example: map(\"QueueName\",\"CertCafeRefeedRequestQueue\")"
  type        = map(string)
  default     = {}
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_evaluate_low_sample_count_percentiles" {
  description = "Evaluate low sample count percentiles of the scale down cloudwatch metric alarm.  Valid values: ignore, evaulate.  Only only applicable for percentile statistics; if autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic is set."
  default     = "evaluate"
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_evaluation_periods" {
  description = "Evaluation periods of the scale down cloudwatch metric alarm.  The number of periods over which data is compared to the specified threshold. If you are setting an alarm which requires that a number of consecutive data points be breaching to trigger the alarm, this value specifies that number. If you are setting an 'M out of N' alarm, this value is the N. An alarm's total current evaluation period can be no longer than one day, so this number multiplied by period cannot be more than 86,400 seconds."
  default     = 3
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic" {
  description = "Extended statistic of the scale down cloudwatch metric alarm.  Required and only applicable for percentile statistics.  Valid values are between p0.0 and p100.  If set overwrites autoscaling_scale_down_cloudwatch_metric_alarm_statistic."
  default     = ""
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_insufficient_data_actions" {
  description = "Data actions of the scale down cloudwatch metric alarm.  Each action is specified as an Amazon Resource Name (ARN).  Can be set to the scale down autoscaling policy via module output."
  type        = list(string)
  default     = []
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_name_suffix" {
  description = "Suffix appended to the scale down cloudwatch metric alarm name."
  default     = "-scale-down"
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_namespace" {
  description = "Namespace of the scale down cloudwatch metric alarm."
  default     = ""
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_metric_name" {
  description = "Metric name of the scale down cloudwatch metric alarm."
  default     = ""
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_period" {
  description = "Period of the scale down cloudwatch metric alarm.  In seconds, over which the specified statistic is applied. Valid values are 10, 30, and any multiple of 60."
  default     = 60
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_ok_actions" {
  description = "Ok actions of the scale down cloudwatch metric alarm.  Each action is specified as an Amazon Resource Name (ARN). Can be set to the scale down autoscaling policy via module output."
  type        = list(string)
  default     = []
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_statistic" {
  description = "Statistic of the scale down cloudwatch metric alarm.  Valid values: SampleCount, Average,Sum, Minimum, Maximum.  For percentile statistics, use autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic.  Not applicable if autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic is set."
  default     = true
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_threshold" {
  description = "Threshold of the scale down cloudwatch metric alarm.  The value against which the specified statistic is compared."
  default     = 0
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_treat_missing_data" {
  description = "Treat missing data of the scale down cloudwatch metric alarm.  Valid values: missing, ignore, breaching and notBreaching."
  default     = "missing"
}

variable "autoscaling_scale_down_cloudwatch_metric_alarm_unit" {
  description = "Unit of the scale down cloudwatch metric alarm. Valid values: Seconds, Microseconds, Milliseconds, Bytes, Kilobytes, Megabytes, Gigabytes, Terabytes, Bits, Kilobits, Megabits, Gigabits, Terabits, Percent, Count, Bytes/Second, Kilobytes/Second, Megabytes/Second, Gigabytes/Second, Terabytes/Second, Bits/Second, Kilobits/Second, Megabits/Second, Gigabits/Second, Terabits/Second, Count/Second, None."
  default     = ""
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_actions_enabled" {
  description = "Actions enabled of the scale up cloudwatch metric alarm."
  default     = true
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_comparison_operator" {
  description = "Comparison Operator of the scale up cloudwatch metric alarm.  Valid values: GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold."
  default     = "GreaterThanThreshold"
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_datapoints_to_alarm" {
  description = "Datapoints to alarm of the scale up cloudwatch metric alarm."
  default     = 2
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_description" {
  description = "Description of the scale up cloudwatch metric alarm."
  default     = "When breached (in alarm state) triggers the auto scaling group to add more ec2 instances."
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_dimensions" {
  description = "Dimensions of the scale up cloudwatch metric alarm.  This is a key value map.  Example:map(\"QueueName\",\"CertCafeRefeedRequestQueue\")"
  type = map(string)
  default = {}
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_evaluate_low_sample_count_percentiles" {
  description = "Evaluate low sample count percentiles of the scale up cloudwatch metric alarm.  Valid values: ignore, evaulate.  Only only applicable for percentile statistics; if autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic is set."
  default     = "evaluate"
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_evaluation_periods" {
  description = "Evaluation periods of the scale up cloudwatch metric alarm.  The number of periods over which data is compared to the specified threshold. If you are setting an alarm which requires that a number of consecutive data points be breaching to trigger the alarm, this value specifies that number. If you are setting an 'M out of N' alarm, this value is the N. An alarm's total current evaluation period can be no longer than one day, so this number multiplied by period cannot be more than 86,400 seconds."
  default     = 3
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_extended_statistic" {
  description = "Extended statistic of the scale up cloudwatch metric alarm.  Required and only applicable for percentile statistics.  Valid values are between p0.0 and p100.  If set overwrites autoscaling_scale_up_cloudwatch_metric_alarm_statistic."
  default     = ""
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_insufficient_data_actions" {
  description = "Data actions of the scale up cloudwatch metric alarm.  Each action is specified as an Amazon Resource Name (ARN).  Can be set to the scale up autoscaling policy via module output."
  type        = list(string)
  default     = []
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_name_suffix" {
  description = "Suffix appended to the scale up cloudwatch metric alarm name."
  default     = "-scale-up"
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_namespace" {
  description = "Namespace of the scale up cloudwatch metric alarm."
  default     = ""
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_metric_name" {
  description = "Metric name of the scale up cloudwatch metric alarm."
  default     = ""
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_period" {
  description = "Period of the scale up cloudwatch metric alarm.  In seconds, over which the specified statistic is applied. Valid values are 10, 30, and any multiple of 60."
  default     = 60
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_ok_actions" {
  description = "Ok actions of the scale up cloudwatch metric alarm.  Each action is specified as an Amazon Resource Name (ARN). Can be set to the scale up autoscaling policy via module output."
  type        = list(string)
  default     = []
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_statistic" {
  description = "Statistic of the scale up cloudwatch metric alarm.  Valid values: SampleCount, Average,Sum, Minimum, Maximum.  For percentile statistics, use autoscaling_scale_up_cloudwatch_metric_alarm_extended_statistic.  Not applicable if autoscaling_scale_up_cloudwatch_metric_alarm_extended_statistic is set."
  default     = ""
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_threshold" {
  description = "Threshold of the scale up cloudwatch metric alarm.  The value against which the specified statistic is compared."
  default     = 1
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_treat_missing_data" {
  description = "Treat missing data of the scale up cloudwatch metric alarm.  Valid values: missing, ignore, breaching and notBreaching."
  default     = "missing"
}

variable "autoscaling_scale_up_cloudwatch_metric_alarm_unit" {
  description = "Unit of the scale up cloudwatch metric alarm. Valid values: Seconds, Microseconds, Milliseconds, Bytes, Kilobytes, Megabytes, Gigabytes, Terabytes, Bits, Kilobits, Megabits, Gigabits, Terabits, Percent, Count, Bytes/Second, Kilobytes/Second, Megabytes/Second, Gigabytes/Second, Terabytes/Second, Bits/Second, Kilobits/Second, Megabits/Second, Gigabits/Second, Terabits/Second, Count/Second, None."
  default     = ""
}

variable "autoscaling_scale_up_autoscaling_policy_adjustment_type" {
  description = "Adjustment type of the scale up autoscaling_policy.  Valid values: ChangeInCapacity ExactCapacity,PercentChangeInCapacity."
  default     = "ChangeInCapacity"
}

variable "autoscaling_scale_up_autoscaling_policy_cooldown" {
  description = "Cooldown of the scale up autoscaling_policy."
  default     = 300
}
variable "autoscaling_scale_up_autoscaling_policy_min_adjustment_magnitude" {
  description = "Min adjustment mangintude type of the scale up autoscaling_policy.  Only applicable if autoscaling_scale_up_autoscaling_policy_adjustment_type is set to PercentChangeInCapacity."
  default     = 1
}

variable "autoscaling_scale_up_autoscaling_policy_name_suffix" {
  description = "Suffix appended to the name of the scale up autoscaling_policy."
  default     = "scale-up"
}

variable "autoscaling_scale_up_autoscaling_policy_scaling_adjustment" {
  description = "Scaling adjustment of the scale up autoscaling_policy."
  default     = 1
}

variable "autoscaling_scale_up_autoscaling_policy_type" {
  description = "Type of the scale up autoscaling_policy.  This module only supports SimpleScaling. StepScaling and TargetTrackingScaling will be added in the future."
  default     = "SimpleScaling"
}

variable "autoscaling_scale_down_autoscaling_policy_adjustment_type" {
  description = "Adjustment type of the scale down autoscaling_policy.  Valid values: ChangeInCapacity ExactCapacity,PercentChangeInCapacity."
  default     = "ChangeInCapacity"
}

variable "autoscaling_scale_down_autoscaling_policy_cooldown" {
  description = "Cooldown of the scale down autoscaling_policy."
  default     = 300
}

variable "autoscaling_scale_down_autoscaling_policy_min_adjustment_magnitude" {
  description = "Min adjustment mangintude type of the scale down autoscaling_policy.  Only applicable if autoscaling_scale_down_autoscaling_policy_adjustment_type is set to PercentChangeInCapacity."
  default     = 1
}

variable "autoscaling_scale_down_autoscaling_policy_name_suffix" {
  description = "Suffix appended to the name of the scale down autoscaling_policy."
  default     = "scale-down"
}

variable "autoscaling_scale_down_autoscaling_policy_scaling_adjustment" {
  description = "Scaling adjustment of the scale down autoscaling_policy."
  default     = -1
}

variable "autoscaling_scale_down_autoscaling_policy_type" {
  description = "Type of the scale down autoscaling_policy.  This module only supports SimpleScaling. StepScaling and TargetTrackingScaling will be added in the future."
  default     = "SimpleScaling"
}



variable "availability_zones" {
  description = "Comma separated list of availability zones for the application.  If application uses a persistent EBS volume then only 1 AZ should be used.  This paramater should be synchronized with asg_subnets."
}

variable "bastion_sg"{
  description = "The bastion host security group.  Allow SSH access to the EC2 node from the bastion."
  default     = ""
}

variable "cloudwatch_alarm_prefix" {
  default = ""
}

variable "contact" {
  description = "The contact address to apply to resources. Should be a valid email address."
}

variable "create_lb_target_group" {
  description = "A bloolean which create an lb target group if set to true.  If create_external_lb is true the lb_target_group will be created regardless of this paramater. See create_external_lb."
  default     = false
}

variable "create_app_ebs" {
  default = false
}

variable "create_app_ebs_nfs_share" {
  default = false
}

variable "create_app_sg" {
  description = "A flag to force creation of the app security group.  Otherwise module with determine if creation is necessary"
  default     = true
}

variable "create_lb" {
  description = "An flag to create an external public internet facing lb.  If true a lb_target_group will automically be created regardless of create_lb_target_group.  See create_lb_target_group."
  default     = false
}

variable "create_lb_alternate_route53_record" {
  description = "An flag to create the external lb alternate route53 record.  Allows external lb to point to an alternate dns record. Useful during releases to move all or a percentage of traffic to new endpoints."
  default     = false
}



variable "create_main_record_dns" {
  description = "An flag to create a main dns record."
  default     = true
}

variable "ebs_optimized"{
  default     = true
  description = "whether or not the ec2 is ebs-optimized on creation"
}

variable "enable_cloudwatch_monitoring" {
  default     = false
  description = "Boolean to enable cloudwatch monitoring alarms."
}

variable "enable_autoscaling" {
  description = "Boolean to enable a main dns record.  When enabled the following should be set: scale_up_cloudwatch_metric_alarm_metric_name, scale_up_cloudwatch_metric_alarm_namespace, scale_up_cloudwatch_metric_alarm_dimensions, (scale_up_cloudwatch_metric_alarm_statistic or scale_up_cloudwatch_metric_alarm_extended_statistic), scale_down_cloudwatch_metric_alarm_metric_name, scale_down_cloudwatch_metric_alarm_namespace, scale_down_cloudwatch_metric_alarm_dimensions, (scale_down_cloudwatch_metric_alarm_statistic or scale_down_cloudwatch_metric_alarm_extended_statistic)."
  default     = false
}

variable "enable_autoscaling_scale_down_cloudwatch_metric_alarm" {
  description = "Boolean to enable scale down cloudwatch metric alarm."
  default     = true
}

variable "enable_autoscaling_scale_down_policy" {
  description = "Boolean to enable scale down autoscaling policy."
  default     = true
}

variable "enable_autoscaling_scale_up_cloudwatch_metric_alarm" {
  description = "Boolean to enable scale up cloudwatch metric alarm."
  default     = true
}

variable "enable_autoscaling_scale_up_policy" {
  description = "Boolean to enable scale up autoscaling policy."
  default     = true
}


variable "environment" {
  description = "The environment label to apply to resources."
}

variable "lb_alternate_route53_alias_name" {
  description = "Name of the external lb alternate route53 alias. Required if create_lb_alternate_route53_record and lb_alternate_route53_is_alias are true."
  default     = 0
}

variable "lb_alternate_route53_alias_zone_id" {
  description = "Zone id of the external lb alternate route53 alias. Required if create_lb_alternate_route53_record and lb_alternate_route53_is_alias are true."
  default     = ""
}

variable "lb_alternate_route53_is_alias" {
  description = "Boolean specifying if external lb alternate route53 ia an alias record.  Applicable if create_lb_alternate_route53_record is true.  If true lb_alternate_route53_alias_name and lb_alternate_route53_alias_zone_id are required."
  default     = false
}

variable "lb_alternate_route53_records" {
  description = "Records of the external lb alternate route53."
  type        = list(string)
  default     = [""]
}

variable "lb_alternate_route53_set_identifier" {
  description = "Set identifier of the external lb alternate route53."
  default = "alternate_route53_set_identifier"
}

variable "lb_alternate_route53_ttl" {
  description = "TTL of the external lb alternate route53 record."
  default     = 60
}

variable "lb_alternate_route53_weight" {
  description = "Weight of the external lb alternate route53 record."
  default = 0
}

variable "lb_logging_bucket_prefix" {
  description = "The logging prefix directory for the S3 bucket."
  default     = ""
}

variable "lb_namespace" {
  default = "AWS/ApplicationELB"
}

variable "lb_hhc_comparison_operator" {
  default = "LessThanThreshold"
}

variable "lb_hhc_evaluation_periods" {
  default = 10
}

variable "lb_hhc_metric_name" {
  default = "HealthyHostCount"
}

variable "lb_hhc_period" {
  default = 60
}

variable "lb_hhc_runbook_code" {
  default = "302"
}

variable "lb_hhc_statistic" {
  default = "Average"
}

variable "lb_hhc_threshold" {
  default = 1
}

variable "lb_hhc_treat_missing_data" {
  default = "missing"
}


variable "lb_http5XX_comparison_operator" {
  default = "GreaterThanThreshold"
}

variable "lb_http5XX_evaluation_periods" {
  default = 30
}

variable "lb_http5XX_metric_name" {
  default = "HTTPCode_ELB_5XX_Count"
}

variable "lb_http5XX_period" {
  default = 60
}

variable "lb_http5XX_runbook_code" {
  default = "305"
}

variable "lb_http5XX_statistic" {
  default = "Sum"
}

variable "lb_http5XX_threshold" {
  default = 25
}

variable "lb_http5XX_treat_missing_data" {
  default = "notBreaching"
}

variable "lb_route53_eval_target_health" {
  description = "Set to true if you want Route 53 to determine whether to respond to DNS queries using this resource record set by checking the health of the resource record set."
  default = false
}

variable "lb_route53_primary_zone_id" {
  description = "The ID of the hosted zone to contain this record."
  default     = ""
}

variable "lb_route53_record_name" {
  description = "The name of the record."
  default     = ""
}

variable "lb_route53_record_type" {
  description = "Type of the external lb alternate route53 record."
  default     = "A"
}

variable "lb_subnets" {
  description = "Comma-separated list of subnet IDs in which the lb should launch resources."
  default = ""
}

variable "lb_https_listener_port" {
  default = "443"
}

variable "lb_https_listener_protocol" {
  default = "HTTPS"
}

variable "lb_sg_protocol" {
  description = "The protocol for the security group attached to the external lb."
  default     = "TCP"
}

variable "lb_ingress_cidr_blocks" {
  description = "list of cidr blocks to allow inbound for load balancer"
  default = [""]
}

variable "lb_http_listener_port" {
  default = "80"
}

variable "lb_https_listener_default_action_type" {
  default = "forward"
  description = "The default action type for the external lb https listener."
}

variable "lb_http_listener_protocol" {
  default = "HTTP"
}

variable "lb_http_listener_default_action_type" {
  default = "forward"
  description = "The default action type for the external lb http listener."
}

variable "lb_enable_deletion_protection" {
  default     = false
  description = "Flag indicating if lb can be deleted.  If enabled, must disable before deleting lb."
}


variable "lb_logging_bucket" {
  default = ""
}

variable "lb_logging_enabled" {
  description = "Is logging enabled for the external lb."
  default     = true
}

variable "lb_idle_timeout" {
  default = 3600
}

variable "lb_ssl_policy" {
  default = "ELBSecurityPolicy-TLS-1-2-2017-01"
  description = "The AWS SSL policy."
}

variable "lb_ssl_certificate_arn" {
  default = ""
  description = "The SSL certificate ARN to attach to the lb."
}

variable "lb_uhc_comparison_operator" {
  default = "GreaterThanThreshold"
}

variable "lb_uhc_evaluation_periods" {
  default = 10
}

variable "lb_uhc_metric_name" {
  default = "UnHealthyHostCount"
}

variable "lb_uhc_period" {
  default = 60
}

variable "lb_uhc_runbook_code" {
  default = "203"
}

variable "lb_uhc_statistic" {
  default = "Average"
}

variable "lb_uhc_threshold" {
  default = 0
}

variable "lb_uhc_treat_missing_data" {
  default = "missing"
}

variable "lb_unhealthy_threshold" {
  default = 10
}

variable "external_front_end_record_name" {
  default = ""
}

variable "lb_route53_weight" {
  description = "External lb Route53 weight."
  default = 10
}

variable "iam_policy" {
  description = "iam role policy"
}

variable "instance_type" {
  description = "The instance type the ASG should use when launching instances."
}

variable "load_balancer_type" {
  default = "application"
}

variable "main_record_route53_name" {
  default     = ""
  description = "The name of the elb record."
}

variable "main_record_route53_primary_zone_id" {
  default     = ""
  description = "The ID of the hosted zone to contain this record."
}

variable "route53_record_ttl" {
  description = "The TTL of the route53 record."
  default     = 60
}

variable "ip_black_list" {
  type = list(string)
  description = "Blocked list of IPs."
  default = []
}

variable "keypair" {
  description = "The key pair name the ASG should assign when launching instances."
}

variable "product" {
  description = "The product label to apply to resources."
}

variable "role" {
  description = "Role of the resource."
}

variable "rootvol_delete"{
  default     = true
  description = "whether to delete root colume on termination for apps"
}

variable "rootvol_size"{
  default     = 20
  description = "size in GB of the root volume for apps"
}

variable "rootvol_type"{
  default     = "gp2"
  description = "whether to delete root colume on termination for apps"
}

variable "spot_price" {
  description = "The spot price used when launching instances.  If not set on demand instances will be used."
  default = ""
}

variable "ssh_port" {
  description = "The port to allow incoming SSH connections generally from bastion."
  default = "22"
}

variable "ssh_port_protocol" {
  description = "The protocol of the ssh port to allow incoming SSH connections generally from bastion."
  default = "TCP"
}

variable "support_lb_sg_id" {
  description = "Security group id of the support lb."
  default     = ""
}

variable "support_sns_topic_devops_high_priority" {
  default = ""
}

variable "target_group_port" {
  default = 80
}

variable "target_group_health_check_healthy_threshold" {
  default = 2
}

variable "target_group_health_check_path" {
  default = "/"
}

variable "target_group_health_check_interval" {
  default = 30
}

variable "target_group_health_check_matcher" {
  default = "200"
}

variable "target_group_health_check_port" {
  default = 80
}

variable "target_group_health_check_protocol" {
  default = "HTTP"
}

variable "target_group_health_check_timeout" {
  default = 5
}

variable "target_group_health_check_unhealthy_threshold" {
  default = 2
}

variable "target_group_protocol" {
  default = "HTTP"
}

variable "target_group_sg_protocol" {
  description = "The security group protocol for the target group."
  default     = "TCP"
}

variable "user_data_template" {
  default     = ""
  description = "user data template file."
}

variable "vpc" {
  description = "The VPC ID in which resources are being launched."
}

variable "vpc_domain"   {
  description = "The domain of the vpc."
}