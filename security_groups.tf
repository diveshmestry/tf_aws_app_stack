#-------------------------------------------------------------
# Generic base security groups which may be useful
#-------------------------------------------------------------
resource "aws_security_group" "http_sg" {
  name        = "http"
  description = "Allow inbound HTTP IP ranges"
  vpc_id      = aws_vpc.app_vpc.id

  tags = {
    environment = var.vpc_environment_tag
    product    = var.vpc_product_tag
    contact      = var.vpc_contact_tag
  }
}

resource "aws_security_group_rule" "allow_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = var.NACL_cidr_blocks
  security_group_id = aws_security_group.http_sg.id
}

resource "aws_security_group" "https_sg" {
  name        = "https"
  description = "Allow inbound HTTPS"
  vpc_id      = aws_vpc.app_vpc.id

  tags = {
    environment = var.vpc_environment_tag
    product    = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }
}

resource "aws_security_group_rule" "allow_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = var.NACL_cidr_blocks
  security_group_id = aws_security_group.https_sg.id
}

resource "aws_security_group" "ssh_sg" {
  name        = "ssh"
  description = "Allow inbound SSH"
  vpc_id      = aws_vpc.app_vpc.id

  tags = {
    environment = var.vpc_environment_tag
    product    = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }
}

resource "aws_security_group_rule" "allow_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = var.NACL_cidr_blocks
  security_group_id = aws_security_group.ssh_sg.id
}