locals {
  create_app_security_group = var.create_lb || var.create_app_sg ? 1 : 0
}

#-----------------------------------------
# ALB Security Group 
#-----------------------------------------
resource "aws_security_group" "lb" {
  count       = var.create_lb ? 1 : 0
  name        = "${var.app_name}-external-alb-sg-${var.environment}"
  description = "Authorize external access to ALB. Attached to ALB."
  vpc_id      = var.vpc
}


resource "aws_security_group_rule" "lb_allow_ingress_all" {
  count             = var.create_lb ? 1 : 0
  type              = "ingress"
  from_port         = -1
  to_port           = -1
  protocol          = "all"
  security_group_id = aws_security_group.lb[count.index].id
  cidr_blocks       = var.all_cidr_block
}

resource "aws_security_group_rule" "lb_allow_ingress_self" {
  count             = var.create_lb ? 1 : 0
  type              = "ingress"
  from_port         = -1
  to_port           = -1
  protocol          = "all"
  security_group_id = aws_security_group.lb[count.index].id
  self              = true
}



/*resource "aws_security_group_rule" "lb_allow_ingress_https" {
  count             = var.create_lb ? 1 : 0
  type              = "ingress"
  from_port         = var.lb_https_listener_port
  to_port           = var.lb_https_listener_port
  protocol          = var.lb_sg_protocol
  security_group_id = aws_security_group.lb[count.index].id
  cidr_blocks       = var.lb_ingress_cidr_blocks
}
*/


resource "aws_security_group_rule" "lb_allow_ingress_http" {
  count             = var.create_lb ? 1 : 0
  type              = "ingress"
  from_port         = var.lb_http_listener_port
  to_port           = var.lb_http_listener_port
  protocol          = var.lb_sg_protocol
  security_group_id = aws_security_group.lb[count.index].id
  cidr_blocks       = var.all_cidr_block
}

resource "aws_security_group_rule" "lb_allow_egress_tg" {
  count                    = var.create_lb ? 1 : 0
  type                     = "egress"
  from_port                = -1
  to_port                  = -1
  protocol                 = "all"
  security_group_id        = aws_security_group.lb[count.index].id
  cidr_blocks = var.all_cidr_block
}


#--------------------------------------------------------------
# Application Security Group
#--------------------------------------------------------------


resource "aws_security_group" "app" {
  count       = local.create_app_security_group
  name        = "${var.app_name}-sg-${var.environment}"
  description = "Authorize access from external ALB/ELB to application.  Attached to EC2."
  vpc_id      = var.vpc
}

resource "aws_security_group_rule" "app_allow_ingress_from_alb" {
  count                    = var.create_lb ? 1 : 0
  type                     = "ingress"
  from_port                = var.target_group_port
  to_port                  = var.target_group_port
  protocol                 = var.lb_sg_protocol
  security_group_id        = aws_security_group.app[count.index].id
  source_security_group_id = aws_security_group.lb[count.index].id
}

resource "aws_security_group_rule" "app_allow_ssh" {
  count             = var.create_lb ? 1 : 0
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "TCP"
  security_group_id = aws_security_group.app[count.index].id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "app_allow_http" {
  count             = var.create_lb ? 1 : 0
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "TCP"
  security_group_id = aws_security_group.app[count.index].id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "app_allow_efs" {
  count             = var.create_lb ? 1 : 0
  type              = "ingress"
  from_port         = 2049
  to_port           = 2049
  protocol          = "TCP"
  security_group_id = aws_security_group.app[count.index].id
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "app_allow_egress" {
  count             = var.create_app_sg ? 1 : 0
  type              = "egress"
  from_port         = var.app_egress_from_port
  to_port           = var.app_egress_to_port
  protocol          = var.app_egress_protocol
  cidr_blocks       = flatten([var.app_egress_cidr_blocks])
  security_group_id = aws_security_group.app[count.index].id
}

