locals {
  enable_autoscaling_scale_up_cloudwatch_metric_alarm                      = min(var.enable_autoscaling ? 1 : 0,var.enable_autoscaling_scale_up_cloudwatch_metric_alarm ? 1 : 0)
  create_autoscaling_scale_up_cloudwatch_metric_alarm                      = var.autoscaling_scale_up_cloudwatch_metric_alarm_extended_statistic == "" ? local.enable_autoscaling_scale_up_cloudwatch_metric_alarm : 0
  create_autoscaling_scale_up_extended_statistic_cloudwatch_metric_alarm   = var.autoscaling_scale_up_cloudwatch_metric_alarm_extended_statistic != "" ? local.enable_autoscaling_scale_up_cloudwatch_metric_alarm : 0

  enable_autoscaling_scale_down_cloudwatch_metric_alarm                    = min(var.enable_autoscaling ? 1 : 0,var.enable_autoscaling_scale_down_cloudwatch_metric_alarm ? 1 : 0)
  create_autoscaling_scale_down_cloudwatch_metric_alarm                    = var.autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic == "" ? local.enable_autoscaling_scale_down_cloudwatch_metric_alarm : 0
  create_autoscaling_scale_down_extended_statistic_cloudwatch_metric_alarm = var.autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic != "" ? local.enable_autoscaling_scale_down_cloudwatch_metric_alarm : 0
}

#--------------------------------------------------------------
# Scale Up Alarm
#--------------------------------------------------------------
resource "aws_cloudwatch_metric_alarm" "autoscaling_scale_up" {
  count                                 = local.create_autoscaling_scale_up_cloudwatch_metric_alarm
  alarm_name                            = "${local.scaling_resource_name_prefix}${var.autoscaling_scale_up_cloudwatch_metric_alarm_name_suffix}"
  comparison_operator                   = var.autoscaling_scale_up_cloudwatch_metric_alarm_comparison_operator
  evaluation_periods                    = var.autoscaling_scale_up_cloudwatch_metric_alarm_evaluation_periods
  metric_name                           = var.autoscaling_scale_up_cloudwatch_metric_alarm_metric_name
  namespace                             = var.autoscaling_scale_up_cloudwatch_metric_alarm_namespace
  period                                = var.autoscaling_scale_up_cloudwatch_metric_alarm_period
  statistic                             = var.autoscaling_scale_up_cloudwatch_metric_alarm_statistic
  threshold                             = var.autoscaling_scale_up_cloudwatch_metric_alarm_threshold
  actions_enabled                       = var.autoscaling_scale_up_cloudwatch_metric_alarm_actions_enabled
  alarm_actions                         = [element(compact(concat(aws_autoscaling_policy.simple_scale_up.*.arn,aws_autoscaling_policy.simple_scale_up_percent_change_in_capacity.*.arn)),0)]
  alarm_description                     = var.autoscaling_scale_up_cloudwatch_metric_alarm_description
  datapoints_to_alarm                   = var.autoscaling_scale_up_cloudwatch_metric_alarm_datapoints_to_alarm
  dimensions                            = var.autoscaling_scale_up_cloudwatch_metric_alarm_dimensions
  insufficient_data_actions             = var.autoscaling_scale_up_cloudwatch_metric_alarm_insufficient_data_actions
  ok_actions                            = var.autoscaling_scale_up_cloudwatch_metric_alarm_ok_actions
  unit                                  = var.autoscaling_scale_up_cloudwatch_metric_alarm_unit
  treat_missing_data                    = var.autoscaling_scale_up_cloudwatch_metric_alarm_treat_missing_data


}

resource "aws_cloudwatch_metric_alarm" "autoscaling_scale_up_extended_statistic" {
  count                                 = local.create_autoscaling_scale_up_extended_statistic_cloudwatch_metric_alarm
  alarm_name                            = "${local.scaling_resource_name_prefix}${var.autoscaling_scale_up_cloudwatch_metric_alarm_name_suffix}"
  comparison_operator                   = var.autoscaling_scale_up_cloudwatch_metric_alarm_comparison_operator
  evaluation_periods                    = var.autoscaling_scale_up_cloudwatch_metric_alarm_evaluation_periods
  metric_name                           = var.autoscaling_scale_up_cloudwatch_metric_alarm_metric_name
  namespace                             = var.autoscaling_scale_up_cloudwatch_metric_alarm_namespace
  period                                = var.autoscaling_scale_up_cloudwatch_metric_alarm_period
  threshold                             = var.autoscaling_scale_up_cloudwatch_metric_alarm_threshold
  actions_enabled                       = var.autoscaling_scale_up_cloudwatch_metric_alarm_actions_enabled
  alarm_actions                         = [element(compact(concat(aws_autoscaling_policy.simple_scale_up.*.arn,aws_autoscaling_policy.simple_scale_up_percent_change_in_capacity.*.arn)),0)]
  alarm_description                     = var.autoscaling_scale_up_cloudwatch_metric_alarm_description
  datapoints_to_alarm                   = var.autoscaling_scale_up_cloudwatch_metric_alarm_datapoints_to_alarm
  dimensions                            = var.autoscaling_scale_up_cloudwatch_metric_alarm_dimensions
  insufficient_data_actions             = var.autoscaling_scale_up_cloudwatch_metric_alarm_insufficient_data_actions
  ok_actions                            = var.autoscaling_scale_up_cloudwatch_metric_alarm_ok_actions
  unit                                  = var.autoscaling_scale_up_cloudwatch_metric_alarm_unit
  treat_missing_data                    = var.autoscaling_scale_up_cloudwatch_metric_alarm_treat_missing_data
  extended_statistic                    = var.autoscaling_scale_up_cloudwatch_metric_alarm_extended_statistic
  evaluate_low_sample_count_percentiles = var.autoscaling_scale_up_cloudwatch_metric_alarm_evaluate_low_sample_count_percentiles
}

#--------------------------------------------------------------
# Scale Down Alarms
#--------------------------------------------------------------
resource "aws_cloudwatch_metric_alarm" "autoscaling_scale_down" {
  count                                 = local.create_autoscaling_scale_down_cloudwatch_metric_alarm
  alarm_name                            = "${local.scaling_resource_name_prefix}${var.autoscaling_scale_down_cloudwatch_metric_alarm_name_suffix}"
  comparison_operator                   = var.autoscaling_scale_down_cloudwatch_metric_alarm_comparison_operator
  evaluation_periods                    = var.autoscaling_scale_down_cloudwatch_metric_alarm_evaluation_periods
  metric_name                           = var.autoscaling_scale_down_cloudwatch_metric_alarm_metric_name
  namespace                             = var.autoscaling_scale_down_cloudwatch_metric_alarm_namespace
  period                                = var.autoscaling_scale_down_cloudwatch_metric_alarm_period
  statistic                             = var.autoscaling_scale_down_cloudwatch_metric_alarm_statistic
  threshold                             = var.autoscaling_scale_down_cloudwatch_metric_alarm_threshold
  actions_enabled                       = var.autoscaling_scale_down_cloudwatch_metric_alarm_actions_enabled
  alarm_actions                         = [element(compact(concat(aws_autoscaling_policy.simple_scale_down.*.arn,aws_autoscaling_policy.simple_scale_down_percent_change_in_capacity.*.arn)),0)]
  alarm_description                     = var.autoscaling_scale_down_cloudwatch_metric_alarm_description
  datapoints_to_alarm                   = var.autoscaling_scale_down_cloudwatch_metric_alarm_datapoints_to_alarm
  dimensions                            = var.autoscaling_scale_down_cloudwatch_metric_alarm_dimensions
  insufficient_data_actions             = var.autoscaling_scale_down_cloudwatch_metric_alarm_insufficient_data_actions
  ok_actions                            = var.autoscaling_scale_down_cloudwatch_metric_alarm_ok_actions
  unit                                  = var.autoscaling_scale_down_cloudwatch_metric_alarm_unit
  treat_missing_data                    = var.autoscaling_scale_down_cloudwatch_metric_alarm_treat_missing_data
}

resource "aws_cloudwatch_metric_alarm" "autoscaling_scale_down_extended_statistic" {
  count                                 = local.create_autoscaling_scale_down_extended_statistic_cloudwatch_metric_alarm
  alarm_name                            = "${local.scaling_resource_name_prefix}${var.autoscaling_scale_down_cloudwatch_metric_alarm_name_suffix}"
  comparison_operator                   = var.autoscaling_scale_down_cloudwatch_metric_alarm_comparison_operator
  evaluation_periods                    = var.autoscaling_scale_down_cloudwatch_metric_alarm_evaluation_periods
  metric_name                           = var.autoscaling_scale_down_cloudwatch_metric_alarm_metric_name
  namespace                             = var.autoscaling_scale_down_cloudwatch_metric_alarm_namespace
  period                                = var.autoscaling_scale_down_cloudwatch_metric_alarm_period
  threshold                             = var.autoscaling_scale_down_cloudwatch_metric_alarm_threshold
  actions_enabled                       = var.autoscaling_scale_down_cloudwatch_metric_alarm_actions_enabled
  alarm_actions                         = [element(compact(concat(aws_autoscaling_policy.simple_scale_down.*.arn,aws_autoscaling_policy.simple_scale_down_percent_change_in_capacity.*.arn)),0)]
  alarm_description                     = var.autoscaling_scale_down_cloudwatch_metric_alarm_description
  datapoints_to_alarm                   = var.autoscaling_scale_down_cloudwatch_metric_alarm_datapoints_to_alarm
  dimensions                            = var.autoscaling_scale_down_cloudwatch_metric_alarm_dimensions
  insufficient_data_actions             = var.autoscaling_scale_down_cloudwatch_metric_alarm_insufficient_data_actions
  ok_actions                            = var.autoscaling_scale_down_cloudwatch_metric_alarm_ok_actions
  unit                                  = var.autoscaling_scale_down_cloudwatch_metric_alarm_unit
  treat_missing_data                    = var.autoscaling_scale_down_cloudwatch_metric_alarm_treat_missing_data
  extended_statistic                    = var.autoscaling_scale_down_cloudwatch_metric_alarm_extended_statistic
  evaluate_low_sample_count_percentiles = var.autoscaling_scale_down_cloudwatch_metric_alarm_evaluate_low_sample_count_percentiles
}