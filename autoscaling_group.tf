#--------------------------------------------------------------
# Rolling deploy is enabled by changing the name of the ASG
#--------------------------------------------------------------
locals {
  asg_name  = "${var.app_name}-${var.environment}-asg"
  #autoscaling_group_name        = element(compact(concat(aws_autoscaling_group.rolling_deploy.*.name,aws_autoscaling_group.standard_deploy.*.name)),0)
}

#--------------------------------------------------------------
# Rolling Deploy Autoscaling Group
# Rolling deploy is enabled by changing the name of the ASG
#--------------------------------------------------------------
resource "aws_autoscaling_group" "rolling_deploy" {
  count                     = var.asg_rolling_deploy ? 1 : 0
  name                      = aws_launch_configuration.app.name
  availability_zones        =  var.availability_zones
  vpc_zone_identifier       =  var.asg_subnets
  target_group_arns         = [aws_alb_target_group.app.*.arn]
  launch_configuration      = aws_launch_configuration.app.name
  enabled_metrics           = var.asg_enabled_metrics
  max_size                  = var.asg_max
  min_size                  = var.asg_min
  wait_for_elb_capacity     = var.asg_min
  health_check_type         = var.asg_health_check_type
  health_check_grace_period = var.asg_grace_period
  wait_for_capacity_timeout = var.asg_capacity_timeout

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "${var.app_name}-instance-${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "environment"
    value               = var.environment
    propagate_at_launch = true
  }

  tag {
    key                 = "product"
    value               = var.product
    propagate_at_launch = true
  }

  tag {
    key                 = "contact"
    value               = var.contact
    propagate_at_launch = true
  }

  tag {
    key                 = "role"
    value               = var.role
    propagate_at_launch = true
  }
  
  tag {
    key                 = "spot-enabled"
    value               = var.asg_autospot_enabled
    propagate_at_launch = true
  }    
  
}

#--------------------------------------------------------------
# Standard Deploy Autoscaling Group
# Standard deploy is controlled by not changing the ASG name.
#--------------------------------------------------------------
resource "aws_autoscaling_group" "standard_deploy" {
  count                     = var.asg_rolling_deploy ? 0 : 1
  name                      = local.asg_name
  vpc_zone_identifier       = var.asg_subnets
  target_group_arns         = aws_alb_target_group.app.*.arn
  launch_configuration      = aws_launch_configuration.app.name
  enabled_metrics           = var.asg_enabled_metrics
  desired_capacity          = var.asg_desired_capacity
  max_size                  = var.asg_max
  min_size                  = var.asg_min
  suspended_processes       = var.asg_suspended_processes
  wait_for_elb_capacity     = var.asg_min
  health_check_type         = var.asg_health_check_type
  health_check_grace_period = var.asg_grace_period
  wait_for_capacity_timeout = var.asg_capacity_timeout

  lifecycle {
    create_before_destroy = false
  }

  tag {
    key                 = "Name"
    value               = "${var.app_name}-instance-${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "environment"
    value               = var.environment
    propagate_at_launch = true
  }

  tag {
    key                 = "product"
    value               = var.product
    propagate_at_launch = true
  }

  tag {
    key                 = "contact"
    value               = var.contact
    propagate_at_launch = true
  }

  tag {
    key                 = "role"
    value               = var.role
    propagate_at_launch = true
  }
  
  tag {
    key                 = "spot-enabled"
    value               = var.asg_autospot_enabled
    propagate_at_launch = true
  }    
}


