#--------------------------------------------------------------
# Applicatin IAM Role and policy
#--------------------------------------------------------------
resource "aws_iam_instance_profile" "app" {
  name = "${var.app_name}-iamrole-profile-${var.environment}"
  role = aws_iam_role.app.name
}

resource "aws_iam_role" "app" {
  name = "${var.app_name}-iamrole-${var.environment}"
  path = "/"
  assume_role_policy =<<EOF
{
     "Version":"2008-10-17",
     "Statement":[
        {
           "Action":"sts:AssumeRole",
           "Principal":{
              "Service":[
                 "ec2.amazonaws.com"
      ]
        },
           "Effect":"Allow",
           "Sid":""
        }
     ]
  }
  EOF
}

resource "aws_iam_role_policy" "app" {
  name = "${var.app_name}-iamrole-policy-${var.environment}"
  role   = aws_iam_role.app.id
  policy = var.iam_policy
}