locals {
	#ALBs have a max length of 32 characters.  Remove the extra characters from the end of the name if too long.
	desired_lb_name = "${var.app_name}-lb-${var.environment}"
	lb_name         = substr(local.desired_lb_name,0,min(length(local.desired_lb_name),32))
}



resource "aws_lb" "load_balancer" {
  count                      = var.create_lb ? 1 : 0
  name                       = local.lb_name
  internal                   = false
  load_balancer_type         = var.load_balancer_type
  subnets                    = var.lb_subnets
  enable_deletion_protection = var.lb_enable_deletion_protection

  #for application load balancers
  security_groups            = [aws_security_group.lb[count.index].id]
  idle_timeout               = var.lb_idle_timeout
  
  /*access_logs {
    bucket  = var.lb_logging_bucket
    prefix  = var.lb_logging_bucket_prefix
    enabled = var.lb_logging_enabled
  }*/
  
  tags = {
    Name        = "${var.app_name}-lb-${var.environment}"
    environment = var.environment
    product     = var.product
    contact     = var.contact
  }  
}


/*
resource "aws_alb_listener" "https_listener" {
  count              = var.create_lb ? 1 : 0
  load_balancer_arn  = aws_lb.load_balancer[count.index].arn
  port               = var.lb_https_listener_port
  protocol           = var.lb_https_listener_protocol
  ssl_policy         = var.lb_ssl_policy
  certificate_arn    = var.lb_ssl_certificate_arn

  default_action {
     target_group_arn = aws_alb_target_group.app[count.index].arn
     type             = var.lb_https_listener_default_action_type
  }
}
*/

resource "aws_alb_listener" "http_listener" {
  count             = var.create_lb ? 1 : 0
  load_balancer_arn = aws_lb.load_balancer[count.index].arn
  port              = var.lb_http_listener_port
  protocol          = var.lb_http_listener_protocol

  default_action {
    target_group_arn = aws_alb_target_group.app[count.index].arn
    type             = var.lb_http_listener_default_action_type
  }
}
