resource "aws_route53_record" "alb" {
  count   = var.create_lb ? 1 : 0
  zone_id = var.lb_route53_primary_zone_id
  name    = var.lb_route53_record_name
  type    = var.lb_route53_record_type

  weighted_routing_policy {
    weight = var.lb_route53_weight
  }

  set_identifier = aws_lb.load_balancer[count.index].name
  alias {
    name                   = aws_lb.load_balancer[count.index].dns_name
    zone_id                = aws_lb.load_balancer[count.index].zone_id
    evaluate_target_health = var.lb_route53_eval_target_health
  }
}

/*resource "aws_route53_zone" "primary" {
  count = var.create_lb ? 1 : 0
  name = "${var.lb_route53_record_name}.com"
  force_destroy = true
}*/

# Allows external lb to point to an alternate dns record.
# Useful during releases to move all or a percentage of traffic to new endpoints
# Weighted routing routes traffic between aws_route53_record.lb and aws_route53_record.lb_alternate via weights.

locals {
  create_lb_alternate       = var.lb_alternate_route53_is_alias ? 0 : ( var.create_lb_alternate_route53_record ? 1 : 0 )
  create_alb_alternate_alias = var.lb_alternate_route53_is_alias ? ( var.create_lb_alternate_route53_record ? 1 : 0 ) : 0
}

resource "aws_route53_record" "lb_alternate" {
  count   = local.create_lb_alternate
  zone_id = var.lb_route53_primary_zone_id
  name    = var.lb_route53_record_name
  ttl     = var.lb_alternate_route53_ttl
  type    = var.lb_route53_record_type

  weighted_routing_policy {
    weight = var.lb_alternate_route53_weight
  }

  set_identifier = var.lb_alternate_route53_set_identifier
  records        = var.lb_alternate_route53_records
}

resource "aws_route53_record" "lb_alternate_alias" {
  count   = local.create_alb_alternate_alias
  zone_id = var.lb_route53_primary_zone_id
  name    = var.lb_route53_record_name
  type    = var.lb_route53_record_type

  weighted_routing_policy {
    weight = var.lb_alternate_route53_weight
  }

  alias {
    name                   = var.lb_alternate_route53_alias_name
    zone_id                = var.lb_alternate_route53_alias_zone_id
    evaluate_target_health = var.lb_route53_eval_target_health
  }

  set_identifier = var.lb_alternate_route53_set_identifier
}

